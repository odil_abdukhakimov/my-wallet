package com.example.mywallet.dto.request;

import lombok.Data;

@Data
public class UsernamePasswordRequestDto {
    private String username;
    private String password;
}
